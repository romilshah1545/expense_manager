var express = require('express');
var categoryRouter = express.Router();

categoryRouter.route('/list').get(function (req, res, next) {
    var userId = req.query.userId;
    console.log("User Id Found in Query Request:"+userId);
    if(!userId){
        return res.status(400).json({status: 400, message: "User Id not found"})
    }
    con.query('SELECT * FROM category WHERE created_by = ?', [userId], function (error, results, fields) {
        if (error) throw error;
        console.log(results);
        res.json(results);
      });
});

categoryRouter.route('/add').post(function (req, res, next) {
    console.log(req.body);
    var category_name = req.body.name;
    var userId = req.body.userId;
    console.log("Category Name:"+category_name);
    console.log("User Id:"+userId);
    if(!category_name || !userId){
        return res.status(400).json({status: 400, message: "Please provide proper Category Name and User Id."});
    }
    con.query("INSERT INTO category (name, created_by) VALUES (?, ?)",[category_name,userId], function (err, result) {
      if (err) throw err;
      res.json(["Category Added Successfully"]);
    });
});

module.exports = categoryRouter;