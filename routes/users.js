var express = require('express');
var userRoutes = express.Router();
// var secured = require('../middleware/secured');

userRoutes.route('/register').post(function (req, res, next) {
    console.log(req.body);
    var email = req.body.email;
    var name = req.body.name;
    var password = req.body.password;
    if(!email || !name || !password){
        return res.status(400).json({status: 400, message: "Please provide Email and Name"})
    }
    con.query("SELECT * FROM user WHERE email = ?",[email], function (err, result) {
        if (err) throw err;
        if(!result.length){
            con.query("INSERT INTO user (name, email, password) VALUES (?, ?)",[name,emai,password], function (err, result) {
                if (err) return res.status(400).json({status: 400, message: "Error while Adding the User. Please try again."});
                res.json(["User Added Successfully"]);
              });
        }else{
            return res.status(400).json({status: 400, message: "User with the given email already exists."})
        }
    });
});

userRoutes.route('/login').post(function (req, res, next) {
    console.log(req.body);
    var email = req.body.email;
    var password = req.body.password;
    console.log("Email:")
    if(!email || !password){
        return res.status(400).json({status: 400, message: "Please provide Email and Password"})
    }
    con.query("SELECT * FROM user WHERE email = ? AND password = ?",[email,password], function (err, result) {
        if (err) throw err;
        if(result.length){
            var token = uniqid();
            con.query("UPDATE user set token = ? WHERE email = ?",[token,email], function (err, result) {
                if (err) return res.status(400).json({status: 400, message: "Error while Logging in. Please Try Aagain"+err});
                res.json({"userId:":"1","authToken":token});
              });
        }else{
            return res.status(401).json({status: 401, message: "Invalid Credentials"});
        }
    });
});

module.exports = userRoutes;
