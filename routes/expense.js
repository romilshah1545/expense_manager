var express = require('express');
const app = require('..');
var expenseRouter = express.Router();
expenseRouter.route('/list').get(function (req, res, next) {
    var userId = req.query.userId;
    console.log("User Id Found in Query Request:"+userId);
    if(!userId){
        return res.status(400).json({status: 400, message: "User Id not found"})
    }
    con.query('SELECT * FROM expense WHERE created_by = ?', [userId], function (error, results, fields) {
        if (error) throw error;
        console.log(results);
        res.json(results);
      });
});

expenseRouter.route('/add').post(function (req, res, next) {
    console.log(req.body);
    var title = req.body.title;
    var amount = req.body.amount;
    var category_id = req.body.category_id;
    var userId = req.body.userId;
    console.log("User Id:"+userId+" Titlee"+title+" Category_id"+category_id+" Amount"+amount);
    if(!title || !amount || !category_id || !userId){
        return res.status(400).json({status: 400, message: "Invalid Details for adding the expense."})
    }
    con.query("INSERT INTO expense (title,amount,category_id,created_by) VALUES (?, ?, ?, ?)",[title,amount,category_id,userId], function (err, result) {
      if (err) throw err;
      res.json(["Expense Added Successfully"]);
    });
});



module.exports = expenseRouter;