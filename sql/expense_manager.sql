/*
SQLyog Community v13.1.5  (64 bit)
MySQL - 5.7.29-log : Database - expense_manager
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`expense_manager` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;

USE `expense_manager`;

/*Table structure for table `category` */

DROP TABLE IF EXISTS `category`;

CREATE TABLE `category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `CATGRY_CREATED_BY_CONSTRAINT` (`created_by`),
  CONSTRAINT `CATGRY_CREATED_BY_CONSTRAINT` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

/*Data for the table `category` */

insert  into `category`(`id`,`name`,`created_by`) values 
(1,'Sales',1),
(2,'namesss',1),
(3,'namesss',1),
(4,'namesss',1),
(5,'namesss1',1);

/*Table structure for table `expense` */

DROP TABLE IF EXISTS `expense`;

CREATE TABLE `expense` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(25) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `category_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `CATGRY_EXPENSE_CONSTRAINT` (`category_id`),
  KEY `EXPENSE_CREATED_BY_CONSTRAINT` (`created_by`),
  CONSTRAINT `CATGRY_EXPENSE_CONSTRAINT` FOREIGN KEY (`category_id`) REFERENCES `category` (`id`),
  CONSTRAINT `EXPENSE_CREATED_BY_CONSTRAINT` FOREIGN KEY (`created_by`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `expense` */

insert  into `expense`(`id`,`title`,`amount`,`category_id`,`created_by`) values 
(1,'Petrol',100,1,1),
(2,'kharcho1',5000,1,1);

/*Table structure for table `user` */

DROP TABLE IF EXISTS `user`;

CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(25) NOT NULL,
  `email` varchar(25) NOT NULL,
  `password` varchar(25) NOT NULL,
  `token` varchar(25) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

/*Data for the table `user` */

insert  into `user`(`id`,`name`,`email`,`password`,`token`) values 
(1,'Romil','romilshah1545@gmail.com','1','xad'),
(2,'User via API','romilshah@gmail.com','2','swu0hauckt7dkj0i');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
