var express = require("express");
var app = express();
var bodyParser = require('body-parser');  
// Create application/x-www-form-urlencoded parser  
var urlencodedParser = bodyParser.urlencoded({ extended: false })  
app.use(express.static('public'));  
app.use(bodyParser.json());
global.uniqid = require('uniqid');
global.DB = require('./models/db_connection');
global.con = DB.db.dbconnect();
var usersRouter = require('./routes/users.js');
var categoryRouter = require('./routes/category.js');
var expenseRouter = require('./routes/expense.js');
app.use('/user',usersRouter);
app.use('/category',categoryRouter);
app.use('/expense',expenseRouter);
app.listen(3000, () => {
 console.log("Server running on port 3000");
});

module.exports = app;
